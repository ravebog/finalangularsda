import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { CarRegistrationComponent } from './components/car-registration/car-registration.component';
import { CarComponent } from './components/car/car.component';
import { ClientRegistrationComponent } from './components/client-registration/client-registration.component';
import { ClientComponent } from './components/client/client.component';
import { HomeComponent } from './components/home/home.component';
import { RentalAddingComponent } from './components/rental-adding/rental-adding.component';
import { RentalComponent } from './components/rental/rental.component';

const routes: Routes = [
  {
    path: "cars",
    component: CarComponent
  },
  {
    path: "car/registration",
    component: CarRegistrationComponent
  },
  {
    path: "car/:id",
    component: CarRegistrationComponent
  },
  {
    path: "clients",
    component: ClientComponent
  },
  {
    path: "client/registration",
    component: ClientRegistrationComponent
  },
  {
    path: "rentals",
    component: RentalComponent
  },
  {
    path: "rental/adding",
    component: RentalAddingComponent
  },
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "about",
    component: AboutComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
