import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule, Routes } from '@angular/router';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HomeComponent } from './components/home/home.component';
import { CarComponent } from './components/car/car.component';
import { ClientComponent } from './components/client/client.component';
import { RentalComponent } from './components/rental/rental.component';
import { ClientRegistrationComponent } from './components/client-registration/client-registration.component';
import { AboutComponent } from './components/about/about.component';
import { RentalAddingComponent } from './components/rental-adding/rental-adding.component';
import { CarRegistrationComponent } from './components/car-registration/car-registration.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarCardComponent } from './components/car-card/car-card.component';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CarComponent,
    ClientComponent,
    RentalComponent,
    ClientRegistrationComponent,
    AboutComponent,
    RentalAddingComponent,
    CarRegistrationComponent,
    CarCardComponent,


  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
