import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RentalAddingComponent } from './rental-adding.component';

describe('RentalAddingComponent', () => {
  let component: RentalAddingComponent;
  let fixture: ComponentFixture<RentalAddingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RentalAddingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RentalAddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
