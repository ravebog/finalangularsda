import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Car } from 'src/app/models/car';
import { Client } from 'src/app/models/client';
import { Mydate } from 'src/app/models/mydate';
import { CarService } from 'src/app/services/car.service';
import { ClientService } from 'src/app/services/client.service';
import { RentalsService } from 'src/app/services/rentals.service';

@Component({
  selector: 'app-rental-adding',
  templateUrl: './rental-adding.component.html',
  styleUrls: ['./rental-adding.component.scss']
})
export class RentalAddingComponent implements OnInit {
  public rentalReg;
  cars: Car[];
  clients: Client[];
  rentalRegistrationForm: FormGroup;
  error: any;
  validMessage: string = "";
  submitted = false;
  pickUpDate: Mydate;
  dropOffDate: Mydate;
  pickUpDate1: string;
  dropOffDate1: string;
  client: any;
  car: any;


  constructor(private formBuilder: FormBuilder, private rentalService: RentalsService,
    private carService: CarService, private clientService: ClientService, private router: Router) { }

  ngOnInit(): void {
    this.rentalRegistrationForm = this.formBuilder.group({
      clientId: ['', Validators.required],
      carId: ['', Validators.required],
      pickUpDate: ['', Validators.required],
      dropOffDate: ['', Validators.required]
    });
    // this.getRentalRegistration(this.route.snapshot.params.id)
    this.getClients();
    this.getCars();

    console.log("form", this.rentalRegistrationForm.value);
  }

  submitRental() {
    if (this.rentalRegistrationForm.valid) {
      this.submitted = true;
      this.submitNewValues();
      console.log("form", this.rentalRegistrationForm.value);

      this.validMessage = "Your booking has been submited. Thank you !"
      this.rentalService.createRentalRegistration(this.rentalRegistrationForm.value).subscribe(
        data => {
          // console.log("aici", this.rentalRegistrationForm.value);
          this.rentalRegistrationForm.reset();
          this.router.navigateByUrl('/rentals');

          return true;
        },
        error => {
          return this.error = error;
        }
      );
      console.log("form", this.rentalRegistrationForm.value);
    } else {

      this.validMessage = "Please fill out the form before submitting !"
    }

  }

  submitNewValues() {
    this.pickUpDate = this.rentalRegistrationForm.get('pickUpDate').value;
    this.dropOffDate = this.rentalRegistrationForm.get('dropOffDate').value;
    this.client = this.rentalRegistrationForm.get('clientId').value;
    this.car = this.rentalRegistrationForm.get('carId').value;
    this.pickUpDate1 = this.pickUpDate.day + "/" + this.pickUpDate.month + "/" + this.pickUpDate.year;
    this.dropOffDate1 = this.dropOffDate.day + "/" + this.dropOffDate.month + "/" + this.dropOffDate.year;
    this.rentalRegistrationForm.patchValue({ clientId: this.client, carId: this.car, pickUpDate: this.pickUpDate1, dropOffDate: this.dropOffDate1 });

  }

  getCars() {
    this.carService.getAllCars().subscribe(
      data => { this.cars = data },
      err => console.error(err),
      () => console.log('Cars loaded')
    );
  }
  getClients() {
    this.clientService.getAllClients().subscribe(
      data => { this.clients = data },
      err => console.error(err),
      () => console.log('Clients loaded')
    );
  }

  // getRentalRegistration(id: number) {
  //   this.rentalService.getRentalById(id).subscribe(
  //     data => { this.rentalReg = data },
  //     err => console.error(err),
  //     () => console.log('bookings loaded')
  //   );
  // }

}
