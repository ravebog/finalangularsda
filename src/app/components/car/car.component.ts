import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Car } from 'src/app/models/car';
import { CarService } from 'src/app/services/car.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {

  public cars;


  constructor(private carService: CarService, private route: ActivatedRoute, private router: Router) { }

  getCars() {
    this.carService.getAllCars().subscribe(
      data => { this.cars = data },
      err => console.error(err),
      () => console.log('Cars loaded')
    );
    // console.log(this.cars.picture)
  }

  deleteCar(id) {
    const confirmed = confirm(`Are you sure you want to delete  ?`);
    if (confirmed) {
      this.carService.deleteCarById(id).subscribe(

        err => console.error(err),
        () => console.log('Car deleted')

      );

    }
    window.location.reload();
  }

  goToEdit(car: Car) {
    this.router.navigate(['car/', car.id]);

  }

  ngOnInit(): void {
    this.getCars();
  }

}
