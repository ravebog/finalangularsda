import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Car } from 'src/app/models/car';
import { Client } from 'src/app/models/client';
import { CarService } from 'src/app/services/car.service';
import { ClientService } from 'src/app/services/client.service';
import { RentalsService } from 'src/app/services/rentals.service';

@Component({
  selector: 'app-rental',
  templateUrl: './rental.component.html',
  styleUrls: ['./rental.component.scss']
})
export class RentalComponent implements OnInit {

  Rentalform: FormGroup;
  validMessage: String = '';
  public rentals;
  public car: any;
  public client: any;
  cars: Car[];
  clients: Client[];

  constructor(private rentalsService: RentalsService, private route: ActivatedRoute,
    private carService: CarService, private clientService: ClientService) { }

  getRentals() {
    this.rentalsService.getAllRentals().subscribe(
      data => { this.rentals = data },
      err => console.error(err),
      () => console.log('Rentals loaded')
    );
  }

  getCar(id: number) {
    this.carService.getCarById(id).subscribe(
      data => { this.car = data; console.log("car", this.car); },
      err => console.error(err),
      () => console.log("car loaded")
    );
  }
  // getClient(id: number) {
  //   this.clientService.getClientById(id).subscribe(
  //     data => { this.client = data },
  //     err => console.error(err),
  //     () => console.log("client loaded")
  //   );
  // }

  ngOnInit(): void {

    this.getRentals();
    // this.getCar(this.route.snapshot.params.id);
  }

}
