import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Car } from 'src/app/models/car';

@Component({
  selector: 'app-car-card',
  templateUrl: './car-card.component.html',
  styleUrls: ['./car-card.component.scss']
})
export class CarCardComponent implements OnInit {

  constructor() { }





  @Input() car: Car;

  @Output() onEdit = new EventEmitter;

  @Output() onDelete = new EventEmitter;

  ngOnInit(): void {
  }

}
