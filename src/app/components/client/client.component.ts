import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/services/client.service';
@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  public clients;
  constructor(private clientService: ClientService) { }
  getClients() {
    this.clientService.getAllClients().subscribe(
      data => { this.clients = data },
      err => console.error(err),
      () => console.log('Clients loaded')
    );
  }
  deleteClient(id) {
    const confirmed = confirm(`Are you sure you want to delete this client ?`);
    if (confirmed) {
      this.clientService.deleteClientById(id)
        .subscribe(
          err => console.error(err),
          () => console.log('Client deleted')
        );
    }
    window.location.reload();
  }
  ngOnInit(): void {
    this.getClients();
  }
}
