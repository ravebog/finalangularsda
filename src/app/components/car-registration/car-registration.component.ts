import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CarService } from 'src/app/services/car.service';

@Component({
  selector: 'app-car-registration',
  templateUrl: './car-registration.component.html',
  styleUrls: ['./car-registration.component.scss']
})


export class CarRegistrationComponent implements OnInit {

  constructor(private carService: CarService, private formBuilder: FormBuilder,
    private router: Router, private http: HttpClient) { }

  selectedFile = null;

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
  }

  onUpload() {

    const uploadData = new FormData();
    uploadData.append('image', this.selectedFile, this.selectedFile.name);
    return this.http.post('server/cars/car/', uploadData);

  }
  public files: any[];
  carRegistrationForm: FormGroup;
  error: any;
  validMessage: string = "";
  submitted = false;



  ngOnInit(): void {
    this.carRegistrationForm = this.formBuilder.group({
      // picture: ['', Validators.required],
      brand: ['', Validators.required],
      model: ['', Validators.required],
      yearOfProduction: ['', Validators.required],
      color: ['', Validators.required],
      mileage: ['', Validators.required],
      status: ['', Validators.required],
      amount: ['', Validators.required],

    });
  }

  submitCarRegistration() {
    this.submitted = true;

    if (this.carRegistrationForm.valid) {
      this.validMessage = "The new car has been submited !"
      this.carService.createCarRegistration(this.carRegistrationForm.value).subscribe(
        data => {
          this.carRegistrationForm.reset();
          this.router.navigateByUrl('/cars');

          return true;
        },
        error => {
          return this.error = error;
        }
      )
    } else {

      this.validMessage = "Please fill out the form before submitting !"
    }
  }


}
