import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ClientService } from 'src/app/services/client.service';


@Component({
  selector: 'app-client-registration',
  templateUrl: './client-registration.component.html',
  styleUrls: ['./client-registration.component.scss']
})
export class ClientRegistrationComponent implements OnInit {

  clientRegistrationForm: FormGroup;
  error: any;
  validMessage: string = "";
  submitted = false;

  constructor(private clientService: ClientService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.clientRegistrationForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]]

    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.clientRegistrationForm.controls; }

  submitClientRegistration() {
    this.submitted = true;

    if (this.clientRegistrationForm.valid) {
      this.validMessage = "Your registration has been submited. Thank you !"
      this.clientService.createClientRegistration(this.clientRegistrationForm.value).subscribe(
        data => {
          this.clientRegistrationForm.reset();
          this.router.navigateByUrl('/clients');

          return true;
        },
        error => {
          return this.error = error;
        }
      )
    } else {

      this.validMessage = "Please fill out the form before submitting !"
    }
  }

}
