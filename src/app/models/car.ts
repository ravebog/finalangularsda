export class Car {
    id: number;
    picture = [
        "src/assets/pictures/car1.png",
        "src/assets/pictures/car2.png",
        "src/assets/pictures/car3.png",
        "src/assets/pictures/car4.png",
        "src/assets/pictures/car5.png",
        "src/assets/pictures/car6.png",
        "src/assets/pictures/car7.png"
    ];
    brand: string;
    model: string;
    yearOfProduction: number;
    color: string;
    mileage: number;
    status = ["UNAVAILABLE", "AVAILABLE"];
    amount: number;

}
