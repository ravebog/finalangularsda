import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Car } from '../models/car';
const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CarService {
  constructor(private http: HttpClient) { }
  getAllCars(): Observable<Car[]> {
    return this.http.get<Car[]>('server/cars')
  }
  getCarById(id: number) {
    return this.http.get('server/cars/car/' + id)
  }
  createCarRegistration(car) {
    let body = JSON.stringify(car)
    return this.http.post('server/cars/car/', body, httpOptions)
  }
  deleteCarById(id: number) {
    return this.http.delete('server/cars/car/' + id)
  }

}
