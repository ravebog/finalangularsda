import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RentalsService {

  constructor(private http: HttpClient) { }

  getAllRentals() {
    return this.http.get('server/rentals');
  }

  getRentalById(id: number) {
    return this.http.get('server/rentals/rental/' + id);
  }

  createRentalRegistration(rental) {
    let body = JSON.stringify(rental);
    return this.http.post('server/rentals/rental', body, httpOptions);
  }

}
