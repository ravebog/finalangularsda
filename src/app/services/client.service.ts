import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from '../models/client';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  getAllClients(): Observable<Client[]> {
    return this.http.get<Client[]>('server/clients');
  }

  getClientById(id: number) {
    return this.http.get('server/clients/client/' + id);
  }

  createClientRegistration(client) {
    let body = JSON.stringify(client);
    return this.http.post('server/clients/client', body, httpOptions);
  }

  deleteClientById(id: number) {
    return this.http.delete('server/clients/client/' + id)
  }



}
